from behave import *
from selenium import webdriver

@given('I am opening the web page')
def step_impl(context):
    context.browser.visit(context.config.server_url + '/todolist')

@when('I see {number} items in my todolist')
def step_impl(context,number):
    for x in range(int(number)):
        context.browser.fill("item_text", "agenda "+str(x)+"\r")

@then('I see a "{text}" about how busy i am')
def step_impl(context, text):
    assert context.browser.find_by_id("komentarnya").first.text == text
