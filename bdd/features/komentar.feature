Feature: Provide automatic comment base on the number of items
        as a user
        I want to have an automatic comment base on the number tasks
        So I can know how busy i am

        Scenario Outline: opening page
          Given I am opening the web page
          When I see <numberofitems> items in my todolist
          Then I see a <comment> about how busy i am
          Examples: comments
            | numberofitems | comment                 |
            | 0             | "yey, saatnya berlibur" |
            | 3             | "sibuk tapi santai"     |
            | 5             | "semangat, kerja keras!"|
            | 7             | "semangat, kerja keras!"|
            | 9             | "oh tidak"              |
